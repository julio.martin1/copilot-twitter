# Copilot session 12-05-23
## Twitter App
We want to build a mini twitter like App that can **receive and send tweets to all connected users** in real-time. No auth is required.  

- **api**: [ws: a Node.js WebSocket library](https://www.npmjs.com/package/ws)
    - Bonus:
        - Allow to get past tweets
        - Store tweets in db, disk or memory (https://github.com/typicode/lowdb)
        - Filter tweets by user `id`
- **web**: [Writing WebSocket client applications](https://developer.mozilla.org/en-US/docs/Web/API/WebSockets_API/Writing_WebSocket_client_applications)
    - Allow users to choose or write an `id` (username) before sending tweets  
    - Bonus
        - Fetch past tweets
        - Paginate
        - Display Avatar
- Feel free to add or improve as many features as you like

![](./ui.png)

## Setup

### Frontend
- `$ cd <repo>/web`
- `$ npm i`
- `$ npm run dev`
### Backend
- `$ cd <repo>/api`
- `$ npm i`
- `$ npm run start`


### Basic message structure
```json
{
  "user": {
    "id": "foo",
    "avatar": "https://www.gravatar.com/avatar/205e460b479e2e5b48aec07710c08d50",
  },
  "tweet": "Hello Zinklars!"
}
```
